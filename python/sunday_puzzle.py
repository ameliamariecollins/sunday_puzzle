#!/usr/bin/env python3

"""
Brute-force solution to Will Shortz's Sunday Puzzle for January 25, 2009.

    "Think of a word that starts and ends with the letter 'm' as in Mary,
    drop the first 'm,' insert an 'o' somewhere and you'll get a new word
    that means the same thing as the first word. What words are these?"
        — https://www.npr.org/templates/story/story.php?storyId=99851339
"""

with open("/usr/share/dict/words", encoding="utf8") as word_file:
    words_ending_in_m = set(
        word
        for word in map(str.strip, word_file.readlines())  # remove newline
        if word.endswith("m")
    )

for word in words_ending_in_m:
    if word.startswith("m"):
        for position in range(2, len(word)):  # skip first m
            candidate: str = word[1:position] + "o" + word[position:]
            if candidate in words_ending_in_m:
                print(f"{word} → {candidate}")
