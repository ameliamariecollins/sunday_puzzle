!  Will Shortz: Think of a word that starts and ends with the letter 'm'.
!               Drop the first 'm,' insert an 'o' somewhere and you'll get
!               a new word that means [almost] the same thing as the first
!               word. What words are these?
!
!  Amelia Marie Collins (amelia@appliedneural.net):

module sunday_puzzle
    implicit none

    character(64), allocatable :: words(:)          ! main words list
    integer :: num_words=0                          ! number of words in list

contains

    subroutine solve_sunday_puzzle(filename)
        character(len=*), intent(in) :: filename
        integer :: i=1

        call load_words(filename)
        do i = 1, num_words
            if ('m' == words(i)(:1)) then           ! test every word that 
                call test_word(words(i))            !   also begins with 'm'
            endif 
        enddo
    endsubroutine

    ! Skip the initial 'm', programmatically insert an 'o' into
    ! each interstitial position and look the candidate up in the
    ! word list. If it is also a word, it *may* be the solution.
    ! Consideration of the meanings of the words will determine
    ! a true solution.
    subroutine test_word(word)
        character(len=*), intent(in) :: word

        character(:), allocatable :: buf
        character(len_trim(word)+1) :: candidate   ! Leave room for 'o'
        integer :: length=0, pos=1, w=1

        buf = trim(word)
        length = len(buf)
        do pos = 2, length-1                        ! skip first 'm'
            candidate = buf(2:pos)                  ! First part
            candidate(pos:pos) = 'o'                ! 'o'
            candidate(pos+1:length+1) = buf(pos+1:) ! Second part
            do w = 1, num_words
                if (candidate == words(w)) then     ! result is a real word?
                    print '(a,1x,"->",1x,a)', buf, candidate
                endif
            enddo
        enddo
    endsubroutine

    ! Make a list of all words that end in 'm', since both the starting
    ! word and the solution both end in 'm'. (The puzzle only specifies
    ! changes at the beginning and middle of the word, leaving the final
    ! letter unaltered.) This list will therefore contain all starting
    ! words, solutions that happen to yield valid words, and many other
    ! words which cannot be produced by following the rules.
    subroutine load_words(filename)
        character(len=*), intent(in) :: filename

        character(64), allocatable :: words_tmp(:) ! temp for enlargement
        character(64) :: buf
        character(:), allocatable :: word
        integer :: lun, length=0, status=0, words_cap=64

        lun = open_file(filename)
        allocate(words(words_cap))
        do while(status == 0)
            read(lun, *, iostat=status) buf
            if (status == 0) then
                word = trim(buf)
                length = len(word)

                call to_lower(word)

                if (weird_chars(word) .eqv. .true.) then
                    cycle
                endif

                if ('m' /= word(length:)) then      ! ONLY words ending in 'm'
                    cycle
                endif 

                if (num_words >= words_cap) then    ! Enlarge if necessary
                    words_cap = 2 * words_cap
                    allocate(words_tmp(words_cap))
                    words_tmp(:) = words
                    call move_alloc(words_tmp, words)
                endif

                num_words = num_words + 1           ! Add to list
                words(num_words) = word
            endif
        enddo
        close(lun)
    endsubroutine

    function open_file(filename) result(lun)
        character(len=*), intent(in) :: filename
        integer :: lun, status=0

        open(newunit=lun, file=filename, status='old', iostat=status)
        if (status /= 0) then
            error stop 'Could not read words file.'
        endif
    endfunction

    subroutine to_lower(word)
        character(len=*), intent(inout) :: word
        integer :: chr=0, length=0, pos=1
        length = len(word)
        do pos = 1, length                      ! to lowercase
            chr = ichar(word(pos:pos))
            if (chr < 97) then
                word(pos:pos) = char(chr + 32)
            endif
        enddo
    endsubroutine

    function weird_chars(word) result(weird)
        character(len=*), intent(inout) :: word
        logical :: weird

        weird = .true.
        if (verify(word, 'abcdefghijklmnopqrstuvwxyz') == 0) then
            weird = .false.
        endif
    endfunction

end module sunday_puzzle

program main
    use sunday_puzzle
    call solve_sunday_puzzle('/usr/share/dict/words')
endprogram
