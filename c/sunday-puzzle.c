
/*
 *  Think of a word that starts and ends with the letter 'm'.  Drop the
 *  first 'm,' insert an 'o' somewhere and you'll get a new word that means
 *  [almost] the same thing as the first word. What words are these?
 */

#define DICTIONARY_FILE_NAME	"/usr/share/dict/words"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

typedef struct trie_node_t {
     struct trie_node_t *letters[26];
}TrieNode;
#define SIZEOF_TRIE_NODE (sizeof(TrieNode))

static TrieNode *trie_node_new(void) {
    return calloc(1, SIZEOF_TRIE_NODE);
}

static ssize_t trie_node_ascii_to_index(char letter) {
    return (ssize_t)letter - 'a';
}

static TrieNode *trie_node_get_next(TrieNode *node, char letter) {
    ssize_t index = trie_node_ascii_to_index(letter);
    return node->letters[index];
}

static TrieNode *trie_node_add(TrieNode *node, char letter) {
    ssize_t index = trie_node_ascii_to_index(letter);
    TrieNode *new_node = trie_node_new();
    if (!new_node)
	return NULL;
    node->letters[index] = new_node;
    return new_node;
}

static void trie_node_defoliate(TrieNode *node) {
    if (!node)
	return;
    for (size_t i=0; i<26; i++) {
	TrieNode *next = node->letters[i];
	trie_node_defoliate(next);
	free(next);
    }
}

static bool trie_word_add(TrieNode *node, char *word) {
    while (*word) {
	char letter = *word++;
	TrieNode *next = trie_node_get_next(node, letter);
	if (next) {
	    node = next;
	}
	else {
	    node = trie_node_add(node, letter);
	    if (!node)
		return false;
	}
    }
    return true;
}

static bool trie_word_find(TrieNode *node, char *word) {
    while (*word) {
	char letter = *word++;
	TrieNode *next = trie_node_get_next(node, letter);
	if (!next)				// Dead end?
	    return false;			// Search failed
	node = next;				// Or move to next node
    }
    return true;				// Found all letters?
}

__attribute__((noreturn))
static void error(int status, char* msg) {
    perror(msg);
    exit(status);
}

int main(void) {
    FILE *file = fopen(DICTIONARY_FILE_NAME, "r");
    if (!file)
	error(1, DICTIONARY_FILE_NAME);

    // Build data structures
    size_t list_capacity = 64, num_words = 0;
    char **starting_words = malloc(list_capacity * sizeof(char *));
    TrieNode *search_trie = trie_node_new();
    if (!search_trie)
	error(1, "Couldn't build the root node of the search trie.\n");

    // Count suitable words to calculate storage allocation
    char *line_buf = NULL;
    size_t line_buf_len = 0;
    ssize_t word_len = 0;
    while ((word_len = getline(&line_buf, &line_buf_len, file)) > 0) {
	/* Word needs to be long enough to insert an "o" after the
	 * first "m" is skipped. Test is done here to catch the case
	 * where a blank line is read, so this length includes
	 * the newline. */
	if (word_len < 4)
	    continue;

	// Write null over newline. Word is now three characters or fewer.
	line_buf[--word_len] = '\0';

	// Clean string
	bool non_letter = false;
	for (char *pos = line_buf; *pos; ++pos) {
	    *pos |= 0x20;		    // Lowercase
	    if (*pos < 'a' || *pos > 'z') { // Non-letter?
		non_letter = true;	    // Mark for skip
		break;
	    }
	}
	if (non_letter)			    // Skip words with other chars
	    continue;

	/* The starting word begins and ends with 'm', and since the
	 * puzzle involves changes to the beginning ("remove the
	 * firt 'm'") and the middle of the word ("insert an 'o'
	 * somewhere") it follows that the resulting word also
	 * ends in 'm'.  */
	if (line_buf[word_len-1] != 'm')    // Must end with "m"
	    continue;

	// Enlarge starting word list if needed
	if (num_words + 1 > list_capacity) {
	    list_capacity *= 2;		    // Double the size
	    starting_words = realloc(starting_words,
		    list_capacity * sizeof(char *));
	}

	/* Words that also begin with 'm' are used to generate
	 * the candidates. These go in the list of starting words. */
	if (line_buf[0] == 'm')		    // Words to test
	    starting_words[num_words++] = strdup(line_buf);

	/* ALL words, including those use the generate candidates,
	 * are added to the search trie, in case a word in the
	 * dictionary begins with 'mm'. */
	if (!trie_word_add(search_trie, line_buf))
	    error(1, "Could not add word.\n");
    }
    free(line_buf);
    fclose(file);

    /* For each starting word (those which begin and end in 'm'),
     * skip the beginning 'm' and programmatically insert an 'o'
     * in every possible position between letters, and look up
     * the result in the search trie. If it is found, then this
     * word was also present in the dictionary and is a valid word.
     * Determination of similar meaning will lead to a solution. */
    for (size_t word_num=0; word_num < num_words; word_num++) {
	char *word = starting_words[word_num] + 1;  // skip the initial "m"
	size_t length = strlen(word);
	char *buf = calloc(length+1, sizeof(char));
	if (!buf)
	    error(1, "Could not allocate buffer for candidate words\n");

	for (size_t pos=1; pos<length; pos++) {	    // For each position,
	    strlcpy(buf, word, pos+1);		    // copy first part,
	    *(buf+pos) = 'o';			    // insert letter 'o',
	    strcpy(buf+pos+1, word+pos);	    // copy last part.
	    if (trie_word_find(search_trie, buf))   // Is it a real word?
		printf("m%s → %s\n", word, buf);
	}
	free(buf);
    }	

    // Deallocate word list
    while (num_words)
	free(starting_words[--num_words]);
    free(starting_words);

    // Deallocate trie
    trie_node_defoliate(search_trie);
    free(search_trie);

    return 0;
}
