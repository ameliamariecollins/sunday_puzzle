/*
 *  NPR Sunday Edition, Sunday Puzzle for January 25, 2009
 *
 *  "Think of a word that starts and ends with the letter 'm' as in Mary,
 *  drop the first 'm,' insert an 'o' somewhere and you'll get a new word
 *  that means the same thing as the first word. What words are these?"
 *
 *  Challenge posed:
 *    https://www.npr.org/templates/story/story.php?storyId=99851339
 *  Answer revealed:
 *    https://www.npr.org/templates/story/story.php?storyId=100111714
 *
 *  A brute force search will return:
 *    maxim => axiom
 *    minium => ionium
 *    mytacism => yotacism
 *
 *  The correct answer (which requires analysis of meaning and is thus
 *  beyond the scope of this program) is "maxim, axiom."
 */

use std::collections::BTreeSet;
use std::fs::File;
use std::io::{self, BufRead};

static DICT_FILE_NAME: &str = "/usr/share/dict/words";

fn main() -> io::Result<()> {
    let words = get_words(DICT_FILE_NAME)?;
    for word in &words {
        // For source words, only use words that start AND end with M.
        if !word.starts_with("m") {
            continue;
        }
        // Make a new candidate word by dropping the M and
        // inserting an O at each position.
        for pos in 2..word.len() {
            // Is candidate word a valid English word?
            let new_word = make_new_word(&word, pos);
            if words.contains(&new_word) {
                println!("{} → {}", word, new_word);
            }
        }
    }

    Ok(())
}

fn get_words(filename: &str) -> io::Result<BTreeSet<String>> {
    let mut words = BTreeSet::new();
    let file = File::open(filename)?;
    for line in io::BufReader::new(file).lines() {
        let word = line?;
        if word.len() > 2 && word.ends_with("m") {
            words.insert(word);
        }
    }

    Ok(words)
}

fn make_new_word(word: &String, pos: usize) -> String {
    // Effectively drop the first M by starting the leftward
    // slice at position 1. Insert an "o". Append remainder of word.
    // word[1..pos].to_string() + "o" + &word[pos..].to_string()
    word[1..pos].to_string() + "o" + &word[pos..].to_string()
}
