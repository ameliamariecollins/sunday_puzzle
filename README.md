
# Brute force solution for the Sunday Puzzle by Will Shortz for January 25, 2009
## Broadcast on the show NPR Sunday Edition 

This puzzle has become my "Hello, world." Occasionally, I add an
implementation in some computer language.

"Think of a word that starts and ends with the letter 'm' as in Mary,
drop the first 'm,' insert an 'o' somewhere and you'll get a new word
that means the same thing as the first word. What words are these?"

Challenge posed:
    https://www.npr.org/templates/story/story.php?storyId=99851339

Answer revealed:
    https://www.npr.org/templates/story/story.php?storyId=100111714

A brute force search will return:

    maxim => axiom
    minium => ionium
    mytacism => yotacism

The correct answer (which requires analysis of meaning and is thus
beyond the scope of these programs) is "maxim, axiom."
